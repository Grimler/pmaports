# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kclock
pkgver=0_git20200905
pkgrel=0
_commit="940e00c0128c8e04626dc7aa39a6d5ee7e2cc474"
pkgdesc="Clock app for Plasma Mobile"
url="https://invent.kde.org/plasma-mobile/kclock"
arch="all !armhf" # armhf blocked by qt5-qtdeclarative
license="LicenseRef-KDE-Accepted-GPL"
depends="
	kirigami-addons
	kirigami2
	"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kcoreaddons-dev
	kdbusaddons-dev
	ki18n-dev
	kirigami2-dev
	knotifications-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtmultimedia-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	"
source="https://invent.kde.org/plasma-mobile/kclock/-/archive/$_commit/kclock-$_commit.tar.gz"
options="!check" # No tests
builddir="$srcdir/$pkgname-$_commit"

prepare() {
	default_prepare

	# qmlplugindump fails for armv7+qemu (pmb#1970). This is purely for
	# packager knowledge and doesn't affect runtime, so we can disable it.
	if [ "$CARCH" = "armv7" ]; then
		sed -i "s/ecm_find_qmlmodule/# ecm_find_qmlmodule/g" CMakeLists.txt
	fi
}

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="0f3159356a7f4bc750f17eee11eb390b672dd0c0112c36dafd4106334539886c9d835a24560a6e302c6d72465bab8a8495a981d1e1a97a07994fa4a02029b466  kclock-940e00c0128c8e04626dc7aa39a6d5ee7e2cc474.tar.gz"
