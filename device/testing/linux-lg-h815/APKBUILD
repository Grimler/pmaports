# Kernel config based on: arch/arm64/configs/lineageos_h815_defconfig

pkgname=linux-lg-h815
pkgver=3.10.84
pkgrel=3
pkgdesc="LG G4 (h815) kernel fork"
arch="aarch64"
_carch="arm64"
_flavor="lg-h815"
url="https://kernel.org"
license="GPL2"
options="!strip !check !tracedeps pmb:cross-native"
makedepends="perl sed installkernel bash gmp-dev bc linux-headers elfutils-dev devicepkg-dev gcc6"

# Compiler: this kernel was only tested with GCC6. Feel free to make a merge
# request if you find out that it is booting working with newer GCCs as
# well. See <https://postmarketos.org/vendorkernel> for instructions.
if [ "${CC:0:5}" != "gcc6-" ]; then
	CC="gcc6-$CC"
	HOSTCC="gcc6-gcc"
	CROSS_COMPILE="gcc6-$CROSS_COMPILE"
fi

# Source
_repository="android_kernel_lge_msm8992"
_commit="aad745da91abbdf8bacda189aebf22db048604f5"
_config="config-$_flavor.$arch"
source="
	$pkgname-$_commit.tar.gz::https://github.com/LineageOS/$_repository/archive/$_commit.tar.gz
	$_config
	compiler-gcc6.h
	fix-powerlevel.patch
"
builddir="$srcdir/$_repository-$_commit"

prepare() {
	default_prepare
	. downstreamkernel_prepare
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-postmarketOS"
}

package() {
	downstreamkernel_package "$builddir" "$pkgdir" "$_carch" "$_flavor"
}

sha512sums="48a3345de582a6e4ba7fc2f87ffd423750bb8a1878ce386b8504306a49294e01e6c4c7511d2c178d8c9fb228c288782fe64f606df381843f144986d1cd467ad7  linux-lg-h815-aad745da91abbdf8bacda189aebf22db048604f5.tar.gz
402468eed41f972a6444e49384124588ca36628be20efb5f939c51f28e1c2f00e79ae96c08d2ef73d55c50fc997b6a34c5923b4f6c60f35c848058cf7c01236f  config-lg-h815.aarch64
d80980e9474c82ba0ef1a6903b434d8bd1b092c40367ba543e72d2c119301c8b2d05265740e4104ca1ac5d15f6c4aa49e8776cb44264a9a28dc551e0d1850dcc  compiler-gcc6.h
337fe3e03580bbe73e5c20420981c11bf7c71e67270a828df309e6f08a573c3ce0d5cdd68efe5552abb4635d4ba1788df80e81745204955fedaf5731144ea994  fix-powerlevel.patch"
